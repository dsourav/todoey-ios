//
//  ViewController.swift
//  Todoey
//
//  Created by Sourav on 8/2/23.
//

import UIKit

class TodoListViewController: UITableViewController {
    let localStorageService = LocalStorageService()
    var todoList = [TodoModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        todoList = localStorageService.getAllTodosItem()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = todoList[indexPath.row].title
        cell.accessoryType = todoList[indexPath.row].isCompleted ? .checkmark : .none

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)

        cell?.setSelected(false, animated: true)
        if
            cell?.accessoryType == .checkmark
        {
            cell?.accessoryType = .none
        } else {
            cell?.accessoryType = .checkmark
        }

        let currentTodo = todoList[indexPath.row]
        currentTodo.isCompleted = !currentTodo.isCompleted

        todoList[indexPath.row] = currentTodo
        
        localStorageService.addTodos(todos: todoList)
    }

    // MARK: - Add Todo item section

    @IBAction func onAddTodoPressed(_ sender: UIBarButtonItem) {
        var todoTextField = UITextField()
        let alert = UIAlertController(title: "Add new todo", message: "", preferredStyle: .alert)
        let addAction = UIAlertAction(title: "Add", style: .default) { _ in

            if let item = todoTextField.text {
                let todoModel = TodoModel()
                todoModel.title = item
                self.todoList.append(todoModel)
                self.tableView.reloadData()
                self.localStorageService.addTodos(todos: self.todoList)
            }
        }

        alert.addTextField { textField in

            textField.placeholder = "Enter your text"
            todoTextField = textField
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alert.dismiss(animated: true)
        }
        alert.addAction(addAction)
        alert.addAction(cancelAction)

        present(alert, animated: true)
    }
}
