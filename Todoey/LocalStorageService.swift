//
//  LocalStorageService.swift
//  Todoey
//
//  Created by Sourav on 8/4/23.
//

import Foundation

class LocalStorageService {
    var documentDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("to-do.plist")

    func addTodos(todos: [TodoModel]) {
        do {
            let encoder = PropertyListEncoder()
            let data = try encoder.encode(todos)

            if let dir = documentDir {
                try data.write(to: dir)
            }
        }
        catch (_) {}
    }

    func getAllTodosItem() -> [TodoModel] {
        do {
            let data = try Data(contentsOf: documentDir!)
            let decoder = PropertyListDecoder()

            let list = try decoder.decode([TodoModel].self, from: data)

            return list
        }
        catch (_) {
            return []
        }
    }
}
