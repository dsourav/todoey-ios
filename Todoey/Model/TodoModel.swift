//
//  TodoModel.swift
//  Todoey
//
//  Created by Sourav on 8/4/23.
//

import Foundation

class TodoModel: Codable {
    var title: String = ""
    var isCompleted: Bool = false
}
